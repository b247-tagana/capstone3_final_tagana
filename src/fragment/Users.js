import React, { Component } from 'react';
import Loading from '../components/Loading';

import Swal from 'sweetalert2';
import { BsTrash } from 'react-icons/bs';
import { BsFillKeyFill } from 'react-icons/bs';

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
    };
  }

  componentDidMount() {
    fetch(`${process.env.REACT_APP_API_URL}/users/all`)
      .then(res => res.json())
      .then(data => {
        this.setState({user: data})
        console.log(data)
      })
  }

  handleDeleteClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to delete this Profile?',
      text: 'You will not be able to recover this profile after deletion!',
      iconColor: '#d1495b',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d1495b',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        const userId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/delete`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
          Swal.fire(
            'Deleted!',
            'The Profile has been deleted.',
            'success'
          )
        })
        .catch(error => console.error(error));
        console.log('delete profile with id:', _id);
      }
    })
  }

  handleMakeAdminClick(_id) {
    Swal.fire({
      title: 'Are you sure you want to set this Profile to admin?',
      iconColor: '#d1495b',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d1495b',
      confirmButtonText: 'Yes, make it an Admin!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        const userId = _id;
        fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/setAsAdmin`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(res => res.json())
        .then(data => {
          this.componentDidMount();
          console.log(data);
          Swal.fire(
            'Successful!',
            'The Profile is now an Admin.',
            'success'
          )
        })
        .catch(error => console.error(error));
        console.log('The profile with id:', _id ,'is now an Admin');
      }
    })
  }

  render() {
    const { user } = this.state;

    return (
      <div className="p-5 mt-[12px]">
        <h1 className="text-3xl font-bold mb-[45px]">Users</h1>
        <div className="border-b border-gray-300 mb-8"></div>
        <div className="bg-white p-10 rounded-md shadow-md mx-auto overflow-auto" style={{maxHeight: 'calc(100vh - 180px)'}}>
          {user.map((item) => (
            <div key={item._id} className="border-b mb-8">
              <div className="flex justify-between">
                <span className="text-sm font-medium">User ID: </span>
                <span className="text-sm">{item._id}</span>
              </div>
              <div className="flex justify-between">
                <span className="text-sm font-medium">First name: </span>
                <span className="text-sm">{item.firstName}</span>
              </div>
              <div className="flex justify-between">
                <span className="text-sm font-medium">Last name: </span>
                <span className="text-sm">{item.lastName}</span>
              </div>
              <div className="flex justify-between">
                <span className="text-sm font-medium">Email: </span>
                <span className="text-sm">{item.email}</span>
              </div>
              <div className="flex justify-between">
                <span className="text-sm font-medium">Mobile number: </span>
                <span className="text-sm">{item.mobileNo}</span>
              </div>
              <div className="flex justify-between mb-3">
                <span className="text-sm font-medium">isAdmin: </span>
                <span className="text-sm">{item.isAdmin ? 'Yes' : 'No'}</span>
              </div>
              <div className="flex items-center">
                <button
                  className="bg-[#d1495b] hover:bg-red-500 text-white font-bold py-2 px-4 rounded mb-3 w-[69px] mr-2"
                  onClick={() => this.handleDeleteClick(item._id)}
                  title="Delete profile"
                  >
                  <BsTrash className="text-xl text-white align-center justify-center" />
                </button>

                {!item.isAdmin && (
                  <button
                    className="bg-green-600 hover:bg-green-500 text-white font-bold py-2 px-4 rounded mb-3 w-[69px] text-[14px] ml-auto"
                    onClick={() => this.handleMakeAdminClick(item._id)}
                    title="Make admin"
                  >
                    <BsFillKeyFill className="text-xl text-white align-center justify-center" />
                  </button>
                )}
              </div>
            </div>
          ))}
          {user.length === 0 && <Loading />}
        </div>
      </div>
    );
  }
}

export default Users;
