import React, { Component } from 'react';

class UpdateProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      category: props.location.state.productToUpdate.category,
      image: props.location.state.productToUpdate.image,
      name: props.location.state.productToUpdate.name,
      description: props.location.state.productToUpdate.description,
      quantity: props.location.state.productToUpdate.quantity,
      price: props.location.state.productToUpdate.price
    };
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
  };

  handleUpdateClick(_id) {
      const productToUpdate = this.state.products.find(product => product._id === _id);
      this.setState({ productToUpdate });
    }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Category:
          <input type="text" name="category" value={this.state.category} onChange={this.handleChange} />
        </label>
        <label>
          Image:
          <input type="text" name="image" value={this.state.image} onChange={this.handleChange} />
        </label>
        <label>
          Name:
          <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
        </label>
        <label>
          Description:
          <input type="text" name="description" value={this.state.description} onChange={this.handleChange} />
        </label>
        <label>
          Quantity:
          <input type="number" name="quantity" value={this.state.quantity} onChange={this.handleChange} />
        </label>
        <label>
          Price:
          <input type="number" name="price" value={this.state.price} onChange={this.handleChange} />
        </label>
        <button type="submit">Update</button>
      </form>
    );
  }
}

export default UpdateProduct;