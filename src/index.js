import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import ProductProvider from './context/ProductContext';
import CartProvider from './context/CartContext';
import 'bootstrap/dist/css/bootstrap.min.css';

import SidebarProvider from './context/SidebarContext';
import OrderHistoryProvider from './context/OrderHistoryContext';

// Update the import statement for createRoot
import { createRoot } from 'react-dom/client';

const root = createRoot(document.getElementById('root'));
root.render(
  <OrderHistoryProvider>
    <SidebarProvider>
      <CartProvider>
        <ProductProvider>
          <React.StrictMode>
            <App />
          </React.StrictMode>
        </ProductProvider>
      </CartProvider>
    </SidebarProvider>
  </OrderHistoryProvider>
);
