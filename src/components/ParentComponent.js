import React, { useState } from 'react';
import OrderHistory from './OrderHistory';
import Sidebar from './Sidebar';

function ParentComponent() {
  const [checkoutCount, setCheckoutCount] = useState(0);

  const handleCheckout = () => {
    // Increment the checkout count to trigger useEffect in OrderHistory component
    console.log(handleCheckout)
    setCheckoutCount(prevCount => prevCount + 1);
  };

  return (
    <div>
      <OrderHistory checkoutCount={checkoutCount} />
      <Sidebar onCheckout={handleCheckout} />
    </div>
  );
}

export default ParentComponent;