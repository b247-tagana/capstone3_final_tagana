import React, { useContext, useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { BsTrash } from 'react-icons/bs';
import { OrderHistoryContext } from '../context/OrderHistoryContext';
import UserContext from '../UserContext';
import { IoMdArrowForward } from 'react-icons/io';
import Loading from './Loading'

const OrderHistory = ({ checkoutCount }) => {
  const { isOrderOpen, handleOrderClose } = useContext(OrderHistoryContext);
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (user || checkoutCount > 0) {
      setIsLoading(true);
      fetch(`${process.env.REACT_APP_API_URL}/order/${user.id}/order`)
        .then(res => res.json())
        .then(data => {
          setOrders(data);
          setIsLoading(false);
        })
        .catch(error => {
          console.error(error);
          setIsLoading(false);
        });
    }
  }, [user, checkoutCount]);

  const toggleOrderDetails = (orderId) => {
    setOrders(prevOrders => {
      return prevOrders.map(order => {
        if (order._id === orderId) {
          return { ...order, isOpen: !order.isOpen };
        } else {
          return order;
        }
      });
    });
  };

  const formatDate = (dateString) => {
    const options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      hour12: true
    };
    const date = new Date(dateString);
    const formattedDate = date.toLocaleString(undefined, options);
    return formattedDate;
  };

  return (
    <div className={`${isOrderOpen ? 'right-0' : '-right-full'} w-full bg-white fixed top-0 h-full shadow-2xl md:w-[40vw] xl:max-w-[35vw] transition-all duration-300 z-20 px-4 lg:px-[35px] position:fixed bottom-0 z-10`}>
      <div className="flex items-center justify-between py-6 border-b mb-4">
        <div className="uppercase text-sm font-semibold">Order History</div>
        <div onClick={handleOrderClose} className="cursor-pointer w-8 h-8 flex justify-center items-center">
          <IoMdArrowForward className="text-2xl" />
        </div>
      </div>
      <div className="flex flex-col gap-y-2 h-[820px] lg:h-[820px] overflow-y-auto overflow-x-hidden">
        {isLoading ? (
          <div>Loading...</div>
        ) : (
          orders.length > 0 ? (
            orders.map((order) => (
              <div key={order._id} className="mb-3">
                <div className="py-3 cursor-pointer bg-gray-200 bg-opacity-80 text-black rounded-md px-[12px] py-4" onClick={() => toggleOrderDetails(order._id)}>
                  <div className="flex justify-between">
                    <div className="flex items-center ml-2" style={{ minWidth: '150px' }}>
                      <span className="text-sm">Order ID: {order._id}</span>
                    </div>
                    <div className="flex items-center mr-2">
                      <span className="text-sm">{formatDate(order.purchasedOn)}</span>
                    </div>
                </div>
            </div>
            
            {order.isOpen && (
            <div className="bg-gray-100 bg-opacity-20 px-10 py-6 rounded-md shadow-md my-4">
             <div className="mb-2 mt-2">
                      {order.products.map((product) => (
                        <div key={product._id} className="flex justify-between">
                          <span className="text-sm">( x{product.quantity} ) {product.name}</span>
                          <span className="text-sm">{product.price}</span>
                        </div>
                      ))}
                      <div className="border-b mt-3"></div>
                    </div>
                    <div className="flex justify-between mb-2">
                      <span className="text-sm font-medium">Total Amount:</span>
                      <span className="text-sm font-medium">${order.totalAmount.toFixed(2)}</span>
                    </div>
                  </div>
                )}
              </div>
            ))
          ) : (
            <div className="text-center mt-4 v-center"><i>Start shopping to see your order history</i></div>
          )
        )}
      </div>
    </div>
  );
}

export default OrderHistory;
