// import ProductCard from '../components/ProductCard';
// import { useState, useEffect } from 'react';

// export default function Products() {
//   const [products, setProducts] = useState([]);
//   const [isLoading, setIsLoading] = useState(true);

//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/products/active`)
//       .then((res) => res.json())
//       .then((data) => {
//         setProducts(
//           data.map((product) => {
//             // Adding quantity property to each product object
//             return { ...product, quantity: 0 };
//           })
//         );
//         setIsLoading(false);
//       })
//       .catch((error) => console.error(error));
//   }, []);

//   const handleIncrement = (productId) => {
//     setProducts(
//       products.map((product) => {
//         if (product._id === productId) {
//           return { ...product, quantity: product.quantity + 1 };
//         }
//         return product;
//       })
//     );
//   };

//   const handleDecrement = (productId) => {
//     setProducts(
//       products.map((product) => {
//         if (product._id === productId && product.quantity > 0) {
//           return { ...product, quantity: product.quantity - 1 };
//         }
//         return product;
//       })
//     );
//   };

//   return (
//     <>
//       <h1>Products</h1>
//       {isLoading ? (
//         <p>Loading...</p>
//       ) : (
//         products.map((product) => (
//           <ProductCard
//             key={product._id}
//             product={product}
//             quantity={product.quantity}
//             onIncrement={handleIncrement}
//             onDecrement={handleDecrement}
//           />
//         ))
//       )}
//     </>
//   );
// }
