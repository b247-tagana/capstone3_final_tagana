import React, { useState, createContext } from 'react';

export const OrderHistoryContext = createContext();

const OrderHistoryProvider = ({ children }) => {
  const [isOrderOpen, setIsOrderOpen] = useState(false);

  const handleOrderClose = () => {
    setIsOrderOpen(false);
  };

  return (
    <OrderHistoryContext.Provider value={{ isOrderOpen, setIsOrderOpen, handleOrderClose }}>
      {children}
    </OrderHistoryContext.Provider>
  );
};

export default OrderHistoryProvider;